void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

}

int x = 5000;

void loop() {
  // put your main code here, to run repeatedly:

  x += random(-300, 300);

  if(x > 10000){
    x = 10000;
  }
  if(x < 1){
    x = 1;
  }
  
  Serial.print(x);
  Serial.print("\r\n");

  String data;

  if(Serial.available()){
    data = Serial.readString();
  }

  delay(20);
}
