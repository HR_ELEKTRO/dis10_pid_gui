#ifndef PINGPONG_H
#define PINGPONG_H

#include <QMainWindow>
#include <QStatusBar>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QGroupBox>

#include <QSerialPort>
#include <QSerialPortInfo>

#include <QDebug>

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QCategoryAxis>
#include <QtCharts/QLegendMarker>
#include <QFileDialog>
#include <QMessageBox>

#include <QTimer>
#include <QDateTime>

#include <QtMath>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

QT_USE_NAMESPACE

class PingPong : public QMainWindow
{
    Q_OBJECT

public:
    explicit PingPong(QWidget *parent = 0);
    ~PingPong();

private:
    QSerialPort *m_serialPort;
    double m_currentSetpoint = 50.0;
    qint64 m_startTime = 0;
    qint64 m_settleTime = 0;
    qint64 m_riseTime = 0;

    QList<double> m_hights;
    QList<double> m_setpoints;
    QList<qint64> m_Times;

    /* Serial GUI elements. */
    QComboBox *m_CBserialPortSelect;
    QComboBox *m_CBserialBaudrateSelect;

    /* PID GUI elements. */
    QDoubleSpinBox *m_SBpValue;
    QDoubleSpinBox *m_SBiValue;
    QDoubleSpinBox *m_SBdValue;

    /* Setpoint GUI elements */
    QDoubleSpinBox *m_SBsetpointValue;

    /* PWM GUI elements. */
    QGroupBox      *m_GBpwm;
    QDoubleSpinBox *m_SBpwmValue;

    /* Chart elements. */
    QChartView    *m_CVgraph;
    QChart        *m_CHgraph;
    QValueAxis    *m_VAsamplesAxis;
    QValueAxis    *m_VAtimeAxis;
    QValueAxis    *m_VAdataAxis;
    QCategoryAxis *m_CAlabelAxis;

    /* Chart lines. */
    QLineSeries *m_LSsensorLine;
    QLineSeries *m_LSsetpointLine;
    QLineSeries *m_LSovershootLine;
    QLineSeries *m_LSundershootLine;
    QLineSeries *m_LSstarttimeLine;
    QLineSeries *m_LSstabletimeLine;

    /* Chart control GUI elements. */
    QTimer    *m_TgraphUpdate;
    QSpinBox  *m_SBgraphLength;
    QCheckBox *m_CBpauseGraph;
    QCheckBox *m_CBplotSensor;
    QCheckBox *m_CBplotSetpoint;
    QCheckBox *m_CBplotOvershoot;
    QCheckBox *m_CBplotUndershoot;
    QCheckBox *m_CBplotStartpoint;

    /* Measurements GUI elements */
    QGroupBox      *m_GBmeasurements;
    QDoubleSpinBox *m_SBstableMargin;
    QSpinBox       *m_SBstableSamples;
    QLabel         *m_Lundershoot;
    QLabel         *m_Lovershoot;
    QLabel         *m_LstabeTime;
    QLabel         *m_LisStable;
    QLabel         *m_Ldelta;
    QLabel         *m_LcurrentValue;
    QLabel         *m_LcurrentTime;

    void setSpinboxI(QSpinBox *sp, int minv, int maxv, int val, int incr);
    void setSpinboxD(QDoubleSpinBox *sp, double minv, double maxv, double val, double incr, int decimals);

    bool isStable();
    int findFirstPoint(qint64 from);
    double getOvershoot();
    double getUndershoot();
    double getDelta();
    qint64 getSettleTime();
    qint64 getRiseTime();

    QWidget*   createSettingsWidget();
    QGroupBox* createExperimentBox();
    QGroupBox* createSerialBox();
    QGroupBox* createPidBox();
    QGroupBox* createSetpointBox();
    QGroupBox* createPwmBox();

    QWidget*   createDisplayWidget();
    QGroupBox* createGraphBox();
    QGroupBox* createMeasurementsBox();

    void createStatusbar();

private slots:
    void setExperiment(QString exp);
    void getSerialPorts();
    void getSerialBaudrates();
    void serialConnect();
    void serialDisconnect();
    void serialSend(QString msg);
    void serialReceive();
    void sendPID();
    void sendSetpoint();
    void sendPWM();
    void updateGraph() { updateGraph(false); }
    void updateGraph(bool force);
    void setGraphUpdateTime(int time);
    void clearGraph();
    void resetGraph();
    void updateStats();
    void exportGraph();

    void showAboutQt();
};

#endif // PINGPONG_H
