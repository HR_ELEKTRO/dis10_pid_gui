#-------------------------------------------------
#
# Project created by QtCreator 2019-04-19T14:59:42
#
#-------------------------------------------------

QT       += core gui serialport charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PingPong
TEMPLATE = app


SOURCES += main.cpp\
        pingpong.cpp

HEADERS  += pingpong.h

FORMS    += pingpong.ui

