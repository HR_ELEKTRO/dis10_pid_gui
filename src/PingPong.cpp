#include "pingpong.h"
#include "ui_pingpong.h"

PingPong::PingPong(QWidget *parent) :
    QMainWindow(parent)
{
    this->setWindowTitle(tr("PID GUI - Hogeschool Rotterdam"));
    createStatusbar();

    QWidget *mainWidget = new QWidget(this);

    QHBoxLayout *HBmainLayout = new QHBoxLayout();

    HBmainLayout->addWidget(createSettingsWidget());

    QVBoxLayout *VBdata = new QVBoxLayout();
    VBdata->addWidget(createGraphBox());
    VBdata->addSpacing(10);
    VBdata->addWidget(createMeasurementsBox());

    HBmainLayout->addLayout(VBdata);

    mainWidget->setLayout(HBmainLayout);
    this->setCentralWidget(mainWidget);

}

PingPong::~PingPong()
{
    //delete ui;
}

QWidget* PingPong::createSettingsWidget()
{
    QWidget *settingsWidget = new QWidget(this);
    settingsWidget->setMaximumWidth(200);

    QVBoxLayout *VBoxSettings = new QVBoxLayout();
    VBoxSettings->setContentsMargins(0, 0, 0, 0);

    VBoxSettings->addWidget(createExperimentBox());
    VBoxSettings->addSpacing(10);

    VBoxSettings->addWidget(createSerialBox());
    VBoxSettings->addSpacing(10);

    VBoxSettings->addWidget(createPidBox());
    VBoxSettings->addSpacing(10);

    VBoxSettings->addWidget(createSetpointBox());
    VBoxSettings->addSpacing(10);

    VBoxSettings->addWidget(createPwmBox());
    VBoxSettings->addStretch(0);

    settingsWidget->setLayout(VBoxSettings);

    return settingsWidget;
}

QGroupBox* PingPong::createExperimentBox()
{
    QGroupBox *GBexperimentSelect = new QGroupBox(tr("Experiment:"), this);
    QVBoxLayout *VBexperimentSelect = new QVBoxLayout();

    QComboBox *CBExperimentSelect = new QComboBox(this);
    CBExperimentSelect->setToolTip(tr("Select experiment"));
    CBExperimentSelect->addItem(tr("PingPong"));
    CBExperimentSelect->addItem(tr("Temperature"));
    connect(CBExperimentSelect, SIGNAL(currentTextChanged(QString)), this, SLOT(setExperiment(QString)));

    VBexperimentSelect->addWidget(CBExperimentSelect);
    GBexperimentSelect->setLayout(VBexperimentSelect);

    return GBexperimentSelect;
}

QGroupBox* PingPong::createSerialBox()
{
    m_serialPort = NULL;

    QGroupBox *GBserialSettings = new QGroupBox(tr("Serial:"), this);
    QVBoxLayout *VBserialSettings = new QVBoxLayout();

    m_CBserialPortSelect = new QComboBox(this);
    m_CBserialPortSelect->setToolTip(tr("Select serial port."));

    m_CBserialBaudrateSelect = new QComboBox(this);
    m_CBserialBaudrateSelect->setToolTip(tr("Select serial baudrate."));

    QPushButton *PBserialConnect = new QPushButton(tr("Connect"), this);
    PBserialConnect->setToolTip(tr("Connect to selected serial port."));
    connect(PBserialConnect, SIGNAL(released()), this, SLOT(serialConnect()));

    QPushButton *PBserialDisconnect = new QPushButton(tr("Disconnect"), this);
    PBserialDisconnect->setToolTip(tr("Disconnect from serial port."));
    connect(PBserialDisconnect, SIGNAL(released()), this, SLOT(serialDisconnect()));

    QPushButton *PBserialScan = new QPushButton(tr("Scan for ports..."), this);
    PBserialScan->setToolTip(tr("Updates the list of serial ports."));
    connect(PBserialScan, SIGNAL(released()), this, SLOT(getSerialPorts()));

    VBserialSettings->addWidget(m_CBserialPortSelect);
    VBserialSettings->addWidget(m_CBserialBaudrateSelect);
    VBserialSettings->addWidget(PBserialConnect);
    VBserialSettings->addWidget(PBserialDisconnect);
    VBserialSettings->addWidget(PBserialScan);
    GBserialSettings->setLayout(VBserialSettings);

    getSerialBaudrates();
    getSerialPorts();

    return GBserialSettings;
}

QGroupBox* PingPong::createPidBox()
{
    QGroupBox *GBpidSettings = new QGroupBox(tr("PID:"), this);
    QGridLayout *VBpidSettings = new QGridLayout();

    m_SBpValue = new QDoubleSpinBox(this);
    setSpinboxD(m_SBpValue, -20, 20, 1.0, 0.1, 2);
    m_SBiValue = new QDoubleSpinBox(this);
    setSpinboxD(m_SBiValue, -20, 20, 0.0, 0.1, 2);
    m_SBdValue = new QDoubleSpinBox(this);
    setSpinboxD(m_SBdValue, -20, 20, 0.0, 0.1, 2);

    QPushButton *PBpidSend = new QPushButton(tr("Send PID"), this);
    PBpidSend->setToolTip(tr("Sends the PID parameters.\nThe controller in the experiment should be enabled."));
    connect(PBpidSend, SIGNAL(released()), this, SLOT(sendPID()));

    VBpidSettings->addWidget(new QLabel(tr("P:"), this), 1, 1, 1, 1);
    VBpidSettings->addWidget(m_SBpValue, 1, 2, 1, 1);
    VBpidSettings->addWidget(new QLabel(tr("I:"), this), 2, 1, 1, 1);
    VBpidSettings->addWidget(m_SBiValue, 2, 2, 1, 1);
    VBpidSettings->addWidget(new QLabel(tr("D:"), this), 3, 1, 1, 1);
    VBpidSettings->addWidget(m_SBdValue, 3, 2, 1, 1);
    VBpidSettings->addWidget(PBpidSend, 4, 1, 1, 2);
    VBpidSettings->setColumnStretch(2, 2);
    GBpidSettings->setLayout(VBpidSettings);

    return GBpidSettings;
}

QGroupBox* PingPong::createSetpointBox()
{
    m_currentSetpoint = 50.0;

    QGroupBox *GBsetpointSettings = new QGroupBox(tr("Setpoint:"), this);
    QVBoxLayout *VBsetpointSettings = new QVBoxLayout();

    m_SBsetpointValue = new QDoubleSpinBox(this);
    setSpinboxD(m_SBsetpointValue, 0.0, 110.0, m_currentSetpoint, 1.0, 1);

    QPushButton *PBsetpointSend = new QPushButton(tr("Send setpoint"), this);
    PBsetpointSend->setToolTip(tr("Sends the setpoint and sets the startpoint to zero.\nThe controller in the experiment should be enabled."));
    connect(PBsetpointSend, SIGNAL(released()), this, SLOT(sendSetpoint()));

    VBsetpointSettings->addWidget(m_SBsetpointValue);
    VBsetpointSettings->addWidget(PBsetpointSend);
    GBsetpointSettings->setLayout(VBsetpointSettings);

    return GBsetpointSettings;
}

QGroupBox* PingPong::createPwmBox()
{
    m_GBpwm = new QGroupBox(tr("PWM (%):"), this);
    QVBoxLayout *VBpwmSettings = new QVBoxLayout();
    m_SBpwmValue = new QDoubleSpinBox(this);
    setSpinboxD(m_SBpwmValue, 0, 100, 20, 1, 1);
    QPushButton *PBpwmSend = new QPushButton(tr("Send PWM"), this);
    PBpwmSend->setToolTip(tr("Sends the PWM value.\nThe controller in the experiment should be disabled."));
    connect(PBpwmSend, SIGNAL(released()), this, SLOT(sendPWM()));

    VBpwmSettings->addWidget(m_SBpwmValue);
    VBpwmSettings->addWidget(PBpwmSend);
    m_GBpwm->setLayout(VBpwmSettings);

    return m_GBpwm;
}

QGroupBox* PingPong::createGraphBox()
{
    QGroupBox *GBgraph = new QGroupBox(tr("Graph:"), this);
    QVBoxLayout *VBgraph = new QVBoxLayout();
    GBgraph->setMinimumWidth(600);

    /* Create series. */
    m_LSsensorLine = new QLineSeries();
    m_LSsensorLine->setName(tr("Sensor"));
    m_LSsetpointLine = new QLineSeries();
    m_LSsetpointLine->setName(tr("Setpoint"));
    m_LSstarttimeLine = new QLineSeries();
    m_LSstarttimeLine->setName(tr("Startpoint"));
    m_LSovershootLine = new QLineSeries();
    m_LSstabletimeLine = new QLineSeries();
    m_LSstabletimeLine->setName(tr("Stabe"));
    m_LSovershootLine->setName(tr("Overshoot"));
    m_LSundershootLine = new QLineSeries();
    m_LSundershootLine->setName(tr("Undershoot"));

    if(false)
    {
        m_LSsensorLine->setUseOpenGL(true);
        m_LSsetpointLine->setUseOpenGL(true);
        m_LSstarttimeLine->setUseOpenGL(true);
        m_LSovershootLine->setUseOpenGL(true);
        m_LSundershootLine->setUseOpenGL(true);
    }

    /* Create chart and add series. */
    m_CHgraph = new QChart();
    m_CHgraph->addSeries(m_LSstarttimeLine);
    m_CHgraph->addSeries(m_LSstabletimeLine);
    m_CHgraph->addSeries(m_LSovershootLine);
    m_CHgraph->addSeries(m_LSundershootLine);
    m_CHgraph->addSeries(m_LSsetpointLine);
    m_CHgraph->addSeries(m_LSsensorLine);
    m_CHgraph->createDefaultAxes();

    /* Setup Axis. */
    m_VAsamplesAxis = (QValueAxis*)m_CHgraph->axisX();
    m_VAsamplesAxis->setTickCount(11);
    m_VAsamplesAxis->setRange(0, 200);
    m_VAsamplesAxis->setTitleText(tr("Samples"));

    m_VAdataAxis = (QValueAxis*)m_CHgraph->axisY();
    m_VAdataAxis->setTickCount(13);
    m_VAdataAxis->setRange(0, 120);
    m_VAdataAxis->setTitleText(tr("Height (cm)"));

    m_VAtimeAxis = new QValueAxis();
    m_VAtimeAxis->setTitleText("Approximate time since last setpoint update (s)");
    m_VAtimeAxis->setGridLineVisible(false);
    m_VAtimeAxis->setTickCount(11);
    m_VAtimeAxis->setRange(0, 10);
    m_CHgraph->addAxis(m_VAtimeAxis, Qt::AlignTop);

    /*
    m_CAlabelAxis = new QCategoryAxis();
    m_CAlabelAxis->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);
    m_CAlabelAxis->setRange(0, 100);
    m_CAlabelAxis->setGridLineVisible(false);
    m_CAlabelAxis->append(tr("US"), 20);
    m_CAlabelAxis->append(tr("SP"), 50);
    m_CAlabelAxis->append(tr("OS"), 70);
    m_CHgraph->addAxis(m_CAlabelAxis, Qt::AlignRight);
    */

    /* Dummy series for time axis. */
    QLineSeries *ls = new QLineSeries();
    m_CHgraph->addSeries(ls);
    ls->attachAxis(m_VAtimeAxis);
    ls->attachAxis(m_VAdataAxis);
    m_CHgraph->legend()->markers(ls).at(0)->setVisible(false);

    /* Create chartview and add graph. */
    m_CVgraph = new QChartView(m_CHgraph);
    m_CVgraph->setRenderHint(QPainter::Antialiasing);
    m_CVgraph->setRubberBand(QChartView::RectangleRubberBand);
    VBgraph->addWidget(m_CVgraph);

    QHBoxLayout *HBgraphSettings = new QHBoxLayout();

    QPushButton *PBexportGraph = new QPushButton(tr("Export"), this);
    PBexportGraph->setToolTip(tr("Export current graph as image."));
    connect(PBexportGraph, SIGNAL(released()), this, SLOT(exportGraph()));

    QPushButton *PBclearGraph = new QPushButton(tr("Clear"), this);
    PBclearGraph->setToolTip(tr("Clears all aquired data."));
    connect(PBclearGraph, SIGNAL(released()), this, SLOT(clearGraph()));

    QPushButton *PBresetGraph = new QPushButton(tr("Reset"), this);
    PBresetGraph->setToolTip(tr("Resets the graph zoom settings."));
    connect(PBresetGraph, SIGNAL(released()), this, SLOT(resetGraph()));

    m_CBpauseGraph = new QCheckBox(tr("Pause"), this);
    m_CBpauseGraph->setToolTip(tr("Pause graph and measurements updates.\nData aquisition will still continue."));
    m_CBpauseGraph->setChecked(false);

    m_SBgraphLength = new QSpinBox(this);
    m_SBgraphLength->setToolTip(tr("Set sample history length.\nA higher value will increase CPU usage."));
    setSpinboxI(m_SBgraphLength, 10, 5000, 200, 50);

    QSpinBox *SBgraphUpdate = new QSpinBox(this);
    SBgraphUpdate->setToolTip(tr("Set graph and measurements update interval (ms).\nA smaller value will increase CPU usage."));
    setSpinboxI(SBgraphUpdate, 20, 5000, 100, 50);
    connect(SBgraphUpdate, SIGNAL(valueChanged(int)), this, SLOT(setGraphUpdateTime(int)));

    HBgraphSettings->addWidget(PBexportGraph);
    HBgraphSettings->addWidget(PBclearGraph);
    HBgraphSettings->addWidget(m_CBpauseGraph);
    HBgraphSettings->addStretch(0);
    HBgraphSettings->addWidget(new QLabel(tr("Update (ms):")));
    HBgraphSettings->addWidget(SBgraphUpdate);
    HBgraphSettings->addWidget(new QLabel(tr("Length:")));
    HBgraphSettings->addWidget(m_SBgraphLength);
    HBgraphSettings->addWidget(PBresetGraph);

    VBgraph->addLayout(HBgraphSettings);

    QHBoxLayout *HBgraphSettings2 = new QHBoxLayout();
    m_CBplotSensor = new QCheckBox(tr("Sensor"), this);
    m_CBplotSensor->setChecked(true);
    m_CBplotSetpoint = new QCheckBox(tr("Setpoint"), this);
    m_CBplotSetpoint->setChecked(true);
    m_CBplotOvershoot = new QCheckBox(tr("Overshoot"), this);
    m_CBplotOvershoot->setChecked(true);
    m_CBplotUndershoot = new QCheckBox(tr("Undershoot"), this);
    m_CBplotUndershoot->setChecked(true);
    m_CBplotStartpoint = new QCheckBox(tr("Startpoint"), this);
    m_CBplotStartpoint->setChecked(true);

    HBgraphSettings2->addWidget(new QLabel(tr("Plot: "), this));
    HBgraphSettings2->addWidget(m_CBplotStartpoint);
    HBgraphSettings2->addWidget(m_CBplotOvershoot);
    HBgraphSettings2->addWidget(m_CBplotUndershoot);
    HBgraphSettings2->addWidget(m_CBplotSetpoint);
    HBgraphSettings2->addWidget(m_CBplotSensor);
    HBgraphSettings2->addStretch(0);

    VBgraph->addLayout(HBgraphSettings2);

    QHBoxLayout *HBgraphSettings3 = new QHBoxLayout();
    m_LcurrentValue = new QLabel(this);
    m_LcurrentValue->setMinimumWidth(50);
    m_LcurrentValue->setMaximumWidth(50);
    m_LcurrentValue->setText(tr("-"));

    m_LcurrentTime = new QLabel(this);
    m_LcurrentTime->setMinimumWidth(50);
    m_LcurrentTime->setMaximumWidth(50);
    m_LcurrentTime->setText(tr("-"));

    HBgraphSettings3->addWidget(new QLabel(tr("Current value:")));
    HBgraphSettings3->addWidget(m_LcurrentValue);
    HBgraphSettings3->addWidget(new QLabel(tr("Current time:")));
    HBgraphSettings3->addWidget(m_LcurrentTime);
    HBgraphSettings3->addStretch(0);

    VBgraph->addLayout(HBgraphSettings3);

    GBgraph->setLayout(VBgraph);

    m_TgraphUpdate = new QTimer(this);
    setGraphUpdateTime(SBgraphUpdate->value());
    connect(m_TgraphUpdate, SIGNAL(timeout()), this, SLOT(updateGraph()));
    m_TgraphUpdate->start();

    return GBgraph;
}

QGroupBox* PingPong::createMeasurementsBox()
{
    m_GBmeasurements = new QGroupBox(tr("Crappy Measurements:"), this);
    m_GBmeasurements->setCheckable(true);
    m_GBmeasurements->setChecked(true);
    QVBoxLayout *VBstats = new QVBoxLayout();
    QHBoxLayout *HBstatSettings = new QHBoxLayout();

    m_SBstableMargin = new QDoubleSpinBox(this);
    setSpinboxD(m_SBstableMargin, 0, 100.0, 1.0, 0.1, 2);

    m_SBstableSamples = new QSpinBox(this);
    setSpinboxI(m_SBstableSamples, 1, 100, 5, 1);

    HBstatSettings->addWidget(new QLabel(tr("Stable margin (cm):")));
    HBstatSettings->addWidget(m_SBstableMargin);
    HBstatSettings->addStretch(0);
    HBstatSettings->addWidget(new QLabel(tr("Stable samples:")));
    HBstatSettings->addWidget(m_SBstableSamples);

    VBstats->addLayout(HBstatSettings);

    QHBoxLayout *HBstats = new QHBoxLayout();
    HBstats->addWidget(new QLabel(tr("Stable:"), this));
    m_LisStable = new QLabel(this);
    m_LisStable->setMinimumWidth(50);
    m_LisStable->setMaximumWidth(50);
    HBstats->addWidget(m_LisStable);
    HBstats->addStretch(0);
    HBstats->addWidget(new QLabel(tr("Overshoot:"), this));
    m_Lovershoot = new QLabel(this);
    m_Lovershoot->setMinimumWidth(50);
    m_Lovershoot->setMaximumWidth(50);
    HBstats->addWidget(m_Lovershoot);
    HBstats->addStretch(0);
    HBstats->addWidget(new QLabel(tr("Undershoot:"), this));
    m_Lundershoot = new QLabel(this);
    m_Lundershoot->setMinimumWidth(50);
    m_Lundershoot->setMaximumWidth(50);
    HBstats->addWidget(m_Lundershoot);
    HBstats->addStretch(0);
    HBstats->addWidget(new QLabel(tr("Time to stability:"), this));
    m_LstabeTime = new QLabel(this);
    m_LstabeTime->setMinimumWidth(50);
    m_LstabeTime->setMaximumWidth(50);
    HBstats->addWidget(m_LstabeTime);
    HBstats->addWidget(new QLabel(tr("Delta:"), this));
    m_Ldelta = new QLabel(this);
    m_Ldelta->setMinimumWidth(50);
    m_Ldelta->setMaximumWidth(50);
    HBstats->addWidget(m_Ldelta);
    updateStats();

    VBstats->addLayout(HBstats);

    m_GBmeasurements->setLayout(VBstats);

    return m_GBmeasurements;
}

void PingPong::createStatusbar()
{
    QStatusBar *sb = new QStatusBar(this);
    sb->addWidget(new QLabel(tr("Hogeschool Rotterdam - PID GUI")), 1);
    sb->addWidget(new QLabel(tr("Roy Bakker - April 2019")), 1);
    QPushButton *LQt = new QPushButton(tr("Designed with Qt"));
    LQt->setFlat(true);
    connect(LQt, SIGNAL(released()), this, SLOT(showAboutQt()));
    sb->addWidget(LQt, 0);
    this->setStatusBar(sb);
}

void PingPong::setSpinboxI(QSpinBox *sp, int minv, int maxv, int val, int incr)
{
    sp->setMinimum(minv);
    sp->setMaximum(maxv);
    sp->setValue(val);
    sp->setSingleStep(incr);
}

void PingPong::setSpinboxD(QDoubleSpinBox *sp, double minv, double maxv, double val, double incr, int decimals)
{
    sp->setMinimum(minv);
    sp->setMaximum(maxv);
    sp->setValue(val);
    sp->setSingleStep(incr);
    sp->setDecimals(decimals);
}

void PingPong::getSerialPorts()
{
    m_CBserialPortSelect->clear();

    const auto ports = QSerialPortInfo::availablePorts();
    for(QSerialPortInfo port : ports)
    {
        m_CBserialPortSelect->addItem(port.portName() + (port.isNull() ? QObject::tr(" (in use)") : QObject::tr(" (available)")), QVariant(port.systemLocation()));
        if(m_serialPort != NULL && m_serialPort->portName() == port.portName())
        {
            m_CBserialPortSelect->setCurrentIndex(m_CBserialPortSelect->count()-1);
        }
    }    
}

void PingPong::getSerialBaudrates()
{
    m_CBserialBaudrateSelect->clear();
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud115200), QSerialPort::Baud115200);
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud19200), QSerialPort::Baud19200);
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud9600), QSerialPort::Baud9600);
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud1200), QSerialPort::Baud1200);
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud2400), QSerialPort::Baud2400);
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud4800), QSerialPort::Baud4800);
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud38400), QSerialPort::Baud38400);
    m_CBserialBaudrateSelect->addItem(QString::number(QSerialPort::Baud57600), QSerialPort::Baud57600);

}

void PingPong::serialConnect()
{
    if(m_serialPort != NULL) serialDisconnect();

    m_serialPort = new QSerialPort(m_CBserialPortSelect->currentData().toString());
    m_serialPort->setBaudRate(m_CBserialBaudrateSelect->currentData().toInt());
    if(!m_serialPort->open(QIODevice::ReadWrite)){
        QMessageBox::warning(this, tr("Could not connect to serial port"), tr("Could not connect to serialport ") + m_CBserialPortSelect->currentData().toString() + tr("."));
        delete m_serialPort;
        m_serialPort = NULL;
    }
    else
    {
        connect(m_serialPort, SIGNAL(readyRead()), this, SLOT(serialReceive()));
        m_startTime = QDateTime::currentMSecsSinceEpoch();
        m_settleTime = 0;
        m_riseTime = 0;
    }

    getSerialPorts();
}

void PingPong::serialDisconnect()
{
    if(m_serialPort != NULL){
        m_serialPort->close();
        delete m_serialPort;
    }
    m_serialPort = NULL;

    getSerialPorts();
}

void PingPong::serialSend(QString msg)
{
    qDebug() << "Sending: " << msg;

    if(m_serialPort != NULL && m_serialPort->isOpen()){
        m_serialPort->write(msg.toLocal8Bit());
    }
    else{
        qDebug() << "No open port";
    }
}

void PingPong::serialReceive()
{
    while(m_serialPort->canReadLine())
    {
        QByteArray str = m_serialPort->readLine(64);
        qDebug() << str;
        double receivedValue = str.simplified().toDouble()/10;
        qint64 currentTime = QDateTime::currentMSecsSinceEpoch();
        m_Times.append(currentTime);
        m_setpoints.append(m_currentSetpoint);
        m_hights.append(receivedValue);

        while(m_Times.size() > m_SBgraphLength->value()){
            m_Times.removeAt(0);
            m_setpoints.removeAt(0);
            m_hights.removeAt(0);
        }

        m_LcurrentValue->setText(QString::number(receivedValue));
        m_LcurrentTime->setText(QString::number((currentTime - m_startTime) / 1000.0));
    }
}

void PingPong::sendPID()
{
    QString sdata = QString(tr("P:"));
    sdata.append(QString::number((int)(m_SBpValue->value()*100)));
    sdata.append(QString(tr("\r\nI:")));
    sdata.append(QString::number((int)(m_SBiValue->value()*100)));
    sdata.append(QString(tr("\r\nD:")));
    sdata.append(QString::number((int)(m_SBdValue->value()*100)));
    sdata.append(QString(tr("\r\n")));

    serialSend(sdata);
}

void PingPong::sendSetpoint()
{
    m_currentSetpoint = m_SBsetpointValue->value();

    QString sdata = QString(tr("SET:"));
    sdata.append(QString::number((int)(m_currentSetpoint * 10)));
    sdata.append(QString(tr("\r\n")));

    serialSend(sdata);

    if(m_serialPort != NULL && m_serialPort->isOpen()){
        m_serialPort->waitForBytesWritten(1000);
        m_startTime = QDateTime::currentMSecsSinceEpoch();
        m_settleTime = 0;
    }
}

void PingPong::sendPWM()
{
    QString sdata = QString(tr("PWM:"));
    sdata.append(QString::number(m_SBpwmValue->value()));
    sdata.append(QString(tr("\r\n")));

    serialSend(sdata);
}

void PingPong::setExperiment(QString exp)
{
    /*QFile file;
    file.setFileName("experiments.json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QString experiments = file.readAll();
    file.close();*/

    if(exp.contains(tr("PingPong")))
    {
        m_VAdataAxis->setTitleText(tr("Height (cm)"));
        m_GBpwm->setEnabled(true);
    }
    if(exp.contains(tr("Temperature")))
    {
        m_VAdataAxis->setTitleText(tr("Temperature (deg C)"));
        m_GBpwm->setEnabled(false);
    }
}

void PingPong::updateGraph(bool force){

    if(force || !m_CBpauseGraph->isChecked()){
        m_LSsensorLine->clear();
        m_LSsetpointLine->clear();
        m_LSovershootLine->clear();
        m_LSundershootLine->clear();
        m_LSstarttimeLine->clear();
        m_LSstabletimeLine->clear();

        m_LSsensorLine->blockSignals(true);
        m_LSsetpointLine->blockSignals(true);
        m_LSovershootLine->blockSignals(true);
        m_LSundershootLine->blockSignals(true);
        m_LSstarttimeLine->blockSignals(true);
        m_LSstabletimeLine->blockSignals(true);

        double os = getOvershoot();
        double us = getUndershoot();

        for(int i = 0; i < m_hights.size(); i++){
            if(m_CBplotSensor->isChecked()) m_LSsensorLine->append(i, m_hights.at(i));
            if(m_CBplotSetpoint->isChecked()) m_LSsetpointLine->append(i, m_setpoints.at(i));
            if(m_Times.at(i) >= m_startTime)
            {
                if(m_CBplotOvershoot->isChecked()) m_LSovershootLine->append(i, m_currentSetpoint + os);
                if(m_CBplotUndershoot->isChecked()) m_LSundershootLine->append(i, m_currentSetpoint - us);
            }
        }

        if(m_CBplotStartpoint->isChecked() && m_Times.size() > 0 && m_Times.at(0) <= m_startTime){
            int p = findFirstPoint(m_startTime);
            m_LSstarttimeLine->append(p, m_VAdataAxis->min());
            m_LSstarttimeLine->append(p, m_VAdataAxis->max());
        }

        if(m_CBplotStartpoint->isChecked() && m_settleTime > 0 && m_Times.size() > 0 && m_Times.at(0) <= m_startTime + m_settleTime){
            int p = findFirstPoint(m_startTime + m_settleTime);
            m_LSstabletimeLine->append(p, m_VAdataAxis->min());
            m_LSstabletimeLine->append(p, m_VAdataAxis->max());
        }

        if(!m_CHgraph->isZoomed()){
            m_VAsamplesAxis->setMax(m_SBgraphLength->value());
        }

        if(m_Times.size() > 1)
        {
            double startv = m_Times.at(0) - m_startTime;
            double endv = m_Times.at(m_Times.size() - 1) - m_startTime;
            if(m_Times.size() < m_SBgraphLength->value())
            {
                /* Simple method */
                //endv = startv + (((endv - startv) / m_Times.size()) * (m_SBgraphLength->value() + 1));

                /* Complex method */
                double sum = 0.0;
                for(int i = 0; i < m_Times.size() - 1; i++)
                {
                    sum += m_Times.at(i + 1) - m_Times.at(i);
                }

                sum /= m_Times.size() - 1;
                endv = startv + (sum * (m_SBgraphLength->value() + 1));

            }
            m_VAtimeAxis->setRange(startv / 1000.0, endv / 1000.0);
        }

        m_LSstarttimeLine->blockSignals(false);
        m_LSstabletimeLine->blockSignals(false);
        m_LSsensorLine->blockSignals(false);
        m_LSsetpointLine->blockSignals(false);
        m_LSovershootLine->blockSignals(false);
        m_LSundershootLine->blockSignals(false);

        if(m_LSstarttimeLine->count() > 0) emit m_LSstarttimeLine->pointAdded(0);
        if(m_LSstabletimeLine->count() > 0) emit m_LSstabletimeLine->pointAdded(0);
        if(m_LSovershootLine->count() > 0) emit m_LSovershootLine->pointAdded(0);
        if(m_LSundershootLine->count() > 0) emit m_LSundershootLine->pointAdded(0);
        if(m_LSsetpointLine->count() > 0) emit m_LSsetpointLine->pointAdded(0);
        if(m_LSsensorLine->count() > 0) emit m_LSsensorLine->pointAdded(0);

        updateStats();
    }
}

void PingPong::setGraphUpdateTime(int time)
{
    m_TgraphUpdate->setInterval(time);
}

void PingPong::clearGraph(){
    m_hights.clear();
    m_setpoints.clear();
    m_Times.clear();
    updateGraph(true);
}

void PingPong::resetGraph()
{
    m_CHgraph->zoomReset();
}

void PingPong::updateStats()
{
    if(!m_GBmeasurements->isChecked()) return;

    if(isStable())
    {
        m_LisStable->setText(tr("Yes"));
    }
    else
    {
        m_LisStable->setText(tr("No"));
    }

    double overshoot = getOvershoot();
    if(overshoot >= 0.0)
    {
        m_Lovershoot->setText(QString::number(overshoot));
    }
    else
    {
        m_Lovershoot->setText(tr("-"));
    }

    double undershoot = getUndershoot();
    if(undershoot >= 0.0)
    {
        m_Lundershoot->setText(QString::number(undershoot));
    }
    else
    {
        m_Lundershoot->setText(tr("-"));
    }

    double delta = getDelta();
    if(delta >= 0.0)
    {
        m_Ldelta->setText(QString::number(delta));
    }
    else
    {
        m_Ldelta->setText(tr("-"));
    }

    if(m_settleTime <= 0.0)
    {
        qint64 timeToStable = getSettleTime();
        if(timeToStable > 0.0)
        {
            m_LstabeTime->setText(QString::number(timeToStable / 1000.0));
            m_settleTime = timeToStable;
        }
        else if(timeToStable < 0.0)
        {
            m_LstabeTime->setText(tr("-"));
        }
    }
}

int PingPong::findFirstPoint(qint64 from)
{
    for(int i = 0; i < m_Times.size(); i++)
    {
        if(m_Times.at(i) >= from){
            return i;
        }
    }
    return 0;
}

bool PingPong::isStable(){
    int start = findFirstPoint(m_startTime);
    int length = m_Times.size();
    int samples = m_SBstableSamples->value();
    double threshold = m_SBstableMargin->value();

    if(length - start > samples){
        for(int i = length - samples - 1; i < length; i++)
        {
            if(fabs(m_hights.at(i) - m_currentSetpoint) > threshold)
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

double PingPong::getOvershoot()
{
    double overShoot = 0.0;

    for(int i = findFirstPoint(m_startTime); i < m_hights.size(); i++)
    {
        if(m_hights.at(i) - m_currentSetpoint > overShoot)
        {
            overShoot = m_hights.at(i) - m_currentSetpoint;
        }
    }

    return overShoot;
}

double PingPong::getUndershoot()
{
    double underShoot = 0.0;

    for(int i = findFirstPoint(m_startTime); i < m_hights.size(); i++)
    {
        if(m_currentSetpoint - m_hights.at(i) > underShoot)
        {
            underShoot = m_currentSetpoint - m_hights.at(i);
        }
    }

    return underShoot;
}

double PingPong::getDelta()
{
    int length = m_hights.size();
    int samples = m_SBstableSamples->value();
    double delta = 0.0;

    if(length > samples)
    {
        double minv = 1000.0;
        double maxv = -1000.0;

        for(int i = length - samples - 1; i < length; i++)
        {
            if(m_hights.at(i) < minv) minv = m_hights.at(i);
            if(m_hights.at(i) > maxv) maxv = m_hights.at(i);
        }
        delta = maxv - minv;
    }

    return delta;
}

qint64 PingPong::getSettleTime(){
    int startSetpoint = findFirstPoint(m_startTime);
    int samples = m_SBstableSamples->value();
    double stableMargin = m_SBstableMargin->value();

    if(startSetpoint > 0)
    {
        for(int i = startSetpoint; i < m_hights.size() - samples; i++)
        {
            bool stable = true;
            for(int j = i; j < i + samples; j++)
            {
                if(fabs(m_hights.at(i) - m_currentSetpoint) > stableMargin)
                {
                    stable = false;
                }
            }
            if(stable)
            {
                return m_Times.at(i) - m_startTime;
            }
        }
    }
    else {
        return 0;
    }

    return -1;
}

qint64 PingPong::getRiseTime()
{
    int startSetpoint = findFirstPoint(m_startTime);

    for(int i = startSetpoint; i < m_hights.size(); i++)
    {
        if((m_hights.at(startSetpoint) <= m_currentSetpoint && m_hights.at(i) >= m_currentSetpoint) || (m_hights.at(startSetpoint) >= m_currentSetpoint && m_hights.at(i) <= m_currentSetpoint))
        {
            return m_Times.at(i) - m_startTime;
        }
    }

    return -1;
}

void PingPong::exportGraph()
{
    bool wasPaused = m_CBpauseGraph->isChecked();
    if(!wasPaused)
    {
        m_CBpauseGraph->setChecked(true);
    }

    QString fileName = QFileDialog::getSaveFileName(this,
           tr("Save Graph as"), "",
           tr("PNG files (*.png) ;; All files (*)"));

    if(!fileName.isEmpty())
    {
        QPixmap p = m_CVgraph->grab();
        if(!p.save(fileName, "PNG"))
        {
            QMessageBox::critical(this, tr("Error saving image"), tr("Image could not be saved"));
        }
    }

    if(!wasPaused)
    {
        m_CBpauseGraph->setChecked(false);
    }
}

void PingPong::showAboutQt()
{
    QMessageBox::aboutQt(this);
}
